// "request" and "response" is an object with property and methods
// We will create two endpoint route for "/greeting" and "/homepage" and will return a response upon accessing
// The "url" property refers to the url or the link in the browser (endpoint)
// baseURI (/), is req.url == "/"

const http = require("http");
const port = 4000;
const server = http.createServer((req, res) => {
    if(req.url == "/greeting"){
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.end("Hello Again");
    } else if(req.url == "/homepage"){
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.end("This is the homepage");
    }else {
        res.writeHead(404, {"Content-Type": "text/plain"});
        res.end("Page not found");
    }
});
server.listen(port);
console.log(`Server is now accessible at localhost:${port}`);

// Mini Activity
// Create another endpoint for the "/homepage" and send a response "This is the homepage"

